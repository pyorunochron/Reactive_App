package com.pyerun.webfluxdemo;

public class Synchro {

    private int sum = 0;

    public synchronized void calculate() {
        setSum(getSum() + 1);
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public static int staticSum = 0;

    public static synchronized void syncStaticCalculate() {
        staticSum += 1;
    }

    public int sepSum = 0;

    public void performSynchronizedTask() {
        synchronized (this) {
            sepSum += 1;
        }
    }
}
