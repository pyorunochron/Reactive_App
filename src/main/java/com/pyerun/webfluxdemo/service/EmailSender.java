package com.pyerun.webfluxdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSender {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendMail() {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo("222303@student.pwr.edu.pl");
        mailMessage.setSubject("Skreslenie z listy studentow");
        mailMessage.setText("Witamy serdencznie, \n\n\n Uprzejmie informuje ze uwalil Pan kurs i studia i elo xD \n\n Z wyrazami szacunku \n\n Prof.dr.hab.mgr.inz Alber Zul");
        javaMailSender.send(mailMessage);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("poszlo");
    }
}
