package com.pyerun.webfluxdemo.model;

public class Point {

    private String date;
    private int x;
    private int y;
    private int z;

    public Point() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    @Override
    public String toString() {
        return "Point{" +
                "date='" + date + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public Point(String date, int x, int y, int z) {
        this.date = date;
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
