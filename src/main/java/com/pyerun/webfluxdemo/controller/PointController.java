package com.pyerun.webfluxdemo.controller;


import com.pyerun.webfluxdemo.model.Point;
import com.pyerun.webfluxdemo.repository.PointRepository;
import com.pyerun.webfluxdemo.service.EmailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class PointController {

    private final PointRepository pointRepository;

    private final EmailSender emailSender;

    @Autowired
    public PointController(PointRepository pointRepository, EmailSender emailSender) {
        this.pointRepository = pointRepository;
        this.emailSender = emailSender;
    }

    @PostMapping("/")
    public Mono<Point> savePoint(@RequestBody Point a) {
        return pointRepository.save(a);
    }

    @GetMapping("/all")
    public Flux<Point> getAll() {
        return pointRepository.findAll();
    }

    @GetMapping("/delete")
    public Mono<Void> delete() {
        return pointRepository.deleteAll();
    }

    @GetMapping("/mail")
    public String sendMail() {
        emailSender.sendMail();
        return "xD";
    }
}
