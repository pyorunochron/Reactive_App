package com.pyerun.webfluxdemo.controller;

import com.pyerun.webfluxdemo.model.Point;
import com.pyerun.webfluxdemo.model.Tweet;
import com.pyerun.webfluxdemo.repository.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.time.Duration;

@Component
public class TweetController {

    private final TweetRepository tweetRepository;

    @Autowired
    public TweetController(TweetRepository tweetRepository) {
        this.tweetRepository = tweetRepository;
    }

    @GetMapping("/tweets")
    public Flux<Tweet> getAllTweets() {
        Flux<Tweet> all = tweetRepository.findAll();
        return tweetRepository.findAll();
    }

    @PostMapping(value = "/tweets")
    public Mono<Tweet> createTweets(@Valid @RequestBody Tweet tweet) {
        return tweetRepository.save(tweet);
    }

    @GetMapping("/tweets/{id}")
    public Mono<ResponseEntity<Tweet>> getTweetById(@PathVariable(value = "id") String tweetId) {
        return tweetRepository
                .findById(tweetId)
                .map(ResponseEntity::ok) // map all of them into ResponseEntity object type. Http Status 200
                .defaultIfEmpty(ResponseEntity.notFound() // if not found, map intro notFound() type of object HttpStatus 404
                        .build());
    }

    @DeleteMapping(value = "/tweets/{id}")
    public Mono<ResponseEntity<Void>> deleteTweet(@PathVariable String tweetId) {
        return tweetRepository.findById(tweetId)
                .flatMap(tweet ->
                        tweetRepository.delete(tweet).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/stream/tweets", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Tweet> streamAllTweets() {
        return tweetRepository.findAll().delayElements(Duration.ofMillis(100));
    }
}
