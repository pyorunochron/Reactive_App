package com.pyerun.webfluxdemo.repository;

import com.pyerun.webfluxdemo.model.Point;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PointRepository extends ReactiveCrudRepository<Point, String> {
}
