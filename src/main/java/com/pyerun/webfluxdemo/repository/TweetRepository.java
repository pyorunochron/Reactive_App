package com.pyerun.webfluxdemo.repository;

import com.pyerun.webfluxdemo.model.Tweet;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TweetRepository extends ReactiveCrudRepository<Tweet, String> {
}
