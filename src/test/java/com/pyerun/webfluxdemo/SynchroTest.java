package com.pyerun.webfluxdemo;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class SynchroTest {


    @Test
    public void givenMultiThread_whenNonSyncMethod() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Synchro synchro = new Synchro();
        IntStream.range(0, 1000)
                .forEach(c -> executorService.submit(synchro::calculate));
        executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
        assertEquals(1000, synchro.getSum());
    }

    @Test
    public void givenMultiThread_whenStaticSyncMethod() throws InterruptedException {
        ExecutorService service = Executors.newCachedThreadPool();
        IntStream.range(0, 1000)
                .forEach(c -> service.submit(Synchro::syncStaticCalculate));
        service.awaitTermination(100, TimeUnit.MILLISECONDS);
        assertEquals(1000, Synchro.staticSum);

    }

    @Test
    public void givenMultiThread_whenBlockSync() throws InterruptedException {
        ExecutorService service = Executors.newCachedThreadPool();
        Synchro synchro = new Synchro();

        IntStream.range(0,1000)
                .forEach(c->service.submit(synchro::performSynchronizedTask));
        service.awaitTermination(100, TimeUnit.MILLISECONDS);
        assertEquals(1000, synchro.sepSum);

    }
}