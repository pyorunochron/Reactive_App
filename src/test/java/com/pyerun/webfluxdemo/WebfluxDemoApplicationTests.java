package com.pyerun.webfluxdemo;

import com.google.gson.Gson;
import com.pyerun.webfluxdemo.model.Point;
import com.pyerun.webfluxdemo.model.Tweet;
import com.pyerun.webfluxdemo.repository.TweetRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebfluxDemoApplicationTests {

//    @Autowired
//    private WebTestClient webTestClient;
//
//    @Autowired
//    TweetRepository tweetRepository;
//
//    @Test
//    public void testCreateTweet() {
//        Tweet tweet = new Tweet("This is a test tweeet");
//        webTestClient.post().uri("/tweets")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .accept(MediaType.APPLICATION_JSON_UTF8)
//                .body(Mono.just(tweet), Tweet.class)
//                .exchange()
//                .expectStatus().isOk()
//                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
//                .expectBody()
//                .jsonPath("$.id").isNotEmpty()
//                .jsonPath("$.text").isEqualTo("This is a test tweeet");
//    }
//
//    @Test
//    public void getAllTweets() {
//        webTestClient.get().uri("/tweets")
//                .accept(MediaType.APPLICATION_JSON_UTF8)
//                .exchange()
//                .expectStatus().isOk()
//                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
//                .expectBodyList(Tweet.class);
//    }
//
//    @Test
//    public void getSingleTweet() {
//        Tweet tweet = tweetRepository.save(new Tweet("Hello world")).block();
//        assert tweet != null;
//        webTestClient.get()
//                .uri("/tweets/{id}", Collections.singletonMap("id", tweet.getId()))
//                .exchange()
//                .expectStatus().isOk()
//                .expectBody()
//                .consumeWith(entityExchangeResult -> Assertions.assertThat(entityExchangeResult.getResponseBody()).isNotNull());
//    }
//
//    @Test
//    public void updateTweet() {
//        Tweet tweet = tweetRepository.save(new Tweet("To be updated")).block();
//        Tweet newTweet = new Tweet("Updated tweet");
//        assert tweet != null;
//        webTestClient.put()
//                .uri("/tweets/{id}", Collections.singletonMap("id", tweet.getId()))
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .accept(MediaType.APPLICATION_JSON_UTF8)
//                .body(Mono.just(newTweet), Tweet.class)
//                .exchange()
//                .expectStatus().isOk()
//                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
//                .expectBody()
//                .jsonPath("$.text").isEqualTo("Updated tweet");
//    }
//}
    @Test
    public void contextLoads(){
        Point point = new Point("today",1,2,3);
        Gson gson = new Gson();

        String json = gson.toJson(point);
        System.out.println(json);
    }
}