package com.pyerun.webfluxdemo.controller;

import com.pyerun.webfluxdemo.model.Point;
import com.pyerun.webfluxdemo.model.Tweet;
import com.pyerun.webfluxdemo.repository.PointRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PointControllerTest {

    private static final int REQUESTS_AMOUNT = 100;
    @Autowired
    PointRepository pointRepository;

    @Autowired
    private WebTestClient webTestClient;


    @Test
    public void passwordBreaker() {
        RestTemplate restTemplate = new RestTemplate();
        String[] allChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ0123456789".split("");
        String URL = "http://35.234.118.22/vuln.php?q=";
        List<Map<String, Long>> appRuns = new ArrayList<>();
        for (int j = 0; j < 3; j++) {
            List<Long> responseTimes = new ArrayList<>();
            Map<String, Long> averageTimes = new HashMap<>();
            System.out.println("Round 1");
            for (String singleChar : allChars) {
                System.out.println("Making " + REQUESTS_AMOUNT + " requests for " + URL + singleChar);
                for (int k = 0; k < REQUESTS_AMOUNT; k++) {
                    long before = System.currentTimeMillis();
                    ResponseEntity<String> forEntity = restTemplate.getForEntity(URL + singleChar, String.class);
                    long after = System.currentTimeMillis();
                    if (forEntity.getStatusCode().is2xxSuccessful()) {
                        responseTimes.add(after - before);
                    }
                }
                System.out.println("Response times : ");
                System.out.println(responseTimes);
                Long average = responseTimes.stream().mapToLong(aLong -> aLong).sum() / responseTimes.size();
                averageTimes.put(String.valueOf(singleChar), average);
                System.out.println("Average response time : " + average);
                responseTimes.clear();
            }
            appRuns.add(averageTimes);
        }
        appRuns.forEach(stringLongMap -> stringLongMap.forEach((s, aLong) -> System.out.println(s + " : " + aLong)));
    }


    @Test
    public void predicateTesting() {
        Point point = new Point("mama", 1, 1, 1);
        Predicate<Point> pointPredicate = p -> p.getDate().contains("mama");
        List<Point> points = new ArrayList<>();
        points.add(point);
        long count = points.stream().filter(pointPredicate).count();
        boolean anyMatch = points.stream().anyMatch(pointPredicate);
        boolean allMatch = points.stream().allMatch(pointPredicate);
        System.out.println(count);
        System.out.println(anyMatch);
        System.out.println(allMatch);
    }

    @Test
    public void savePoint() {
        Point point = new Point("now", 1, 2, 3);
        webTestClient.post().uri("/")
                .body(Mono.just(point), Point.class)
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void getAll() {
        webTestClient.get().uri("/all")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBodyList(Point.class);
    }
}